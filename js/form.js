$(document).ready(function() {
    const divContainer = document.createElement('div');
    divContainer.className = 'container webchat__bubble__content';
    document.body.appendChild(divContainer);

    const btnForm = document.createElement('button');
    btnForm.className = 'btn btn-primary btn-floating';
    btnForm.type = 'button';
    btnForm.setAttribute('id', 'lanzarPolitica');
    btnForm.setAttribute('data-bs-toggle', 'offcanvas');
    btnForm.setAttribute('data-bs-target', '#offcanvasScrolling');
    btnForm.setAttribute('aria-controls', 'offcanvasScrolling');
    btnForm.setAttribute('data-mdb-toggle', 'tooltip');
    btnForm.setAttribute('data-mdb-placement', 'left');
    btnForm.setAttribute('title', 'Realiza una videollamada');
    const iconButton = document.createElement('i');
    iconButton.className = 'fas fa-headset';
    btnForm.appendChild(iconButton);
    divContainer.appendChild(btnForm);

    const divCanvas = document.createElement('div');
    //Borrar el show
    divCanvas.className = 'offcanvas offcanvas-start';
    divCanvas.setAttribute('data-bs-scroll', true);
    divCanvas.setAttribute('data-bs-backdrop', false);
    divCanvas.setAttribute('tabindex', '-1');
    divCanvas.setAttribute('id', 'offcanvasScrolling');
    divCanvas.setAttribute('aria-labelledby', 'offcanvasScrollingLabel');
    divCanvas.style.visibility='visible';
    divContainer.appendChild(divCanvas);

    const divHeaderCanvas = document.createElement('div');
    divHeaderCanvas.className = 'offcanvas-header';
    divCanvas.appendChild(divHeaderCanvas);

    var controlHorario = ControlHorario();

    const containerLogo = document.createElement('div');
    containerLogo.className = 'container-logo';

    const logoSodimac = document.createElement('div');
    logoSodimac.className = 'logo m-2';
    containerLogo.appendChild(logoSodimac);
    divHeaderCanvas.appendChild(containerLogo);

    const btnCloseCanvas = document.createElement('button');
    btnCloseCanvas.className = 'btn-close text-reset';
    btnCloseCanvas.setAttribute('data-bs-dismiss', 'offcanvas');
    btnCloseCanvas.setAttribute('aria-label', 'Close');
    divHeaderCanvas.appendChild(btnCloseCanvas);

    const divCanvasBody = document.createElement('div');
    divCanvasBody.className = 'offcanvas-body';
    divCanvas.appendChild(divCanvasBody);


    const divForm = document.createElement('div');
    divForm.className = 'form';
    divCanvasBody.appendChild(divForm);

    if(!controlHorario){
        var titleHeaderCanvas = document.createElement('h6');
        titleHeaderCanvas.className = 'offcanvas-title';
        titleHeaderCanvas.setAttribute('id', 'offcanvasScrollingLabel');
        titleHeaderCanvas.innerHTML = '<p class="m-5">¡Gracias por comunicarse por nuestro chat! Le informamos '+
        'que nuestro horario de atención es de Lunes a viernes de ' +
        '7am a 18:00pm sabado de 08:00am a 16:00</p>';
        divCanvasBody.appendChild(titleHeaderCanvas);
        divCanvas.classList.add('time-control');
        logoSodimac.className = 'logo m-2';
    } else {

        const divControls = document.createElement('div');
        divControls.className = 'controls';
        divControls.setAttribute('hidden', true);
        divForm.appendChild(divControls);

        const divInconsMedia = document.createElement('div');
        divInconsMedia.className = 'speaker';
        divControls.appendChild(divInconsMedia);

        const tagIUser = document.createElement('i');
        tagIUser.className = 'fas fa-user';
        divInconsMedia.appendChild(tagIUser);

        const tagIAgent = document.createElement('i');
        tagIAgent.className = 'fas fa-chalkboard-teacher';
        divInconsMedia.appendChild(tagIAgent);

        const divMedia = document.createElement('div');
        divMedia.className = 'media ocultar';
        divControls.appendChild(divMedia);

        const controlVideoLocalView = document.createElement('video');
        controlVideoLocalView.setAttribute('muted', 'true');
        controlVideoLocalView.className = 'video-fluid z-depth-1 colum video-call';
        controlVideoLocalView.setAttribute('id', 'localView');
        controlVideoLocalView.setAttribute('autoplay', 'autoplay');
        controlVideoLocalView.setAttribute('controls', 'controls');

        divMedia.appendChild(controlVideoLocalView);
        const controlVideoRemoteView = document.createElement('video');
        controlVideoRemoteView.className = 'video-fluid z-depth-1';
        controlVideoRemoteView.setAttribute('width', '160');
        controlVideoRemoteView.setAttribute('height', '120');
        controlVideoRemoteView.setAttribute('id', 'remoteView');
        controlVideoRemoteView.setAttribute('autoplay', 'autoplay');
        controlVideoRemoteView.setAttribute('controls', 'controls');
        divMedia.appendChild(controlVideoRemoteView);

        const divEncuesta = document.createElement('div');
        divEncuesta.className = 'colum';

        const inputCheckEncuesta = document.createElement('input');
        inputCheckEncuesta.className = 'form-check-input';
        inputCheckEncuesta.setAttribute('type', 'checkbox');
        inputCheckEncuesta.setAttribute('id', 'encuesta');
        inputCheckEncuesta.setAttribute('checked', 'checked');
        divEncuesta.appendChild(inputCheckEncuesta);

        const labelCheckEncuesta = document.createElement('a');
        labelCheckEncuesta.className = 'form-check-label';
        labelCheckEncuesta.innerText = 'Realizar encuesta';
        divEncuesta.appendChild(labelCheckEncuesta);
        divMedia.appendChild(divEncuesta);
        const form = document.createElement('form');
        form.className = 'row g-3 needs-validation form-inputs';
        form.setAttribute('id', 'form_init');
        form.setAttribute('autocomplete', 'off');
        form.setAttribute('novalidate', 'novalidate');
        divCanvasBody.appendChild(form);

        const bannerDivDos = document.createElement('div');
        bannerDivDos.className = 'container-banner';
        const textBannerDivDos = document.createElement('div');

        textBannerDivDos.textContent = 'Bienvenido a nuestro servicio  de atención';
        bannerDivDos.appendChild(textBannerDivDos);

        form.appendChild(bannerDivDos);

        const divHeadCanvasA = document.createElement('div');
        divHeadCanvasA.className = 'col';
        const divHeadCanvasB = document.createElement('div');
        divHeadCanvasB.className = 'col';

        form.appendChild(divHeadCanvasA);
        form.appendChild(divHeadCanvasB);
        // Se añaden todos los hijos de divHeadCanvasA
        //Name
        const divContainerName = document.createElement('div');
        divContainerName.className = 'row';

        const divIconName = document.createElement('div');
        divIconName.className = 'div-icon-name col';

        const divIconUser = document.createElement('div');
        divIconUser.className = 'icon-user-name';
        divIconName.appendChild(divIconUser);

        const divTextUser = document.createElement('div');
        divTextUser.className = 'div-name-user col';
        divTextUser.textContent = 'Nombre';
        divIconName.appendChild(divTextUser);

        const divInputFirstName = document.createElement('div');
        divInputFirstName.className = 'mb-3 col';

        const inputFirstName = document.createElement('input');
        inputFirstName.className = 'form-control input-border';
        inputFirstName.setAttribute('type', 'text');
        inputFirstName.setAttribute('id', 'FirstName');
        inputFirstName.setAttribute('style', '');
        inputFirstName.setAttribute('for', '');
        inputFirstName.setAttribute('maxlength', '50');
        inputFirstName.setAttribute('onkeypress', 'return validateNames(event);');
        inputFirstName.setAttribute('onpaste', 'return false');
        divInputFirstName.appendChild(inputFirstName);

        divContainerName.appendChild(divIconName);
        divContainerName.appendChild(divInputFirstName);

        divHeadCanvasA.appendChild(divContainerName);

        const divContainerLastName = document.createElement('div');
        divContainerLastName.className = 'row';

        const divIconLastName = document.createElement('div');
        divIconLastName.className = 'div-icon-name col';

        const divIconLastUser = document.createElement('div');
        divIconLastUser.className = 'icon-user-name';
        divIconLastName.appendChild(divIconLastUser);

        const divTextLastUser = document.createElement('div');
        divTextLastUser.className = 'div-name-user col';
        divTextLastUser.textContent = 'Apellidos';
        divIconLastName.appendChild(divTextLastUser);

        const divInputLastName = document.createElement('div');
        divInputLastName.className = 'mb-3 col';

        const inputLastName = document.createElement('input');
        inputLastName.className = 'form-control input-border';
        inputLastName.setAttribute('type', 'text');
        inputLastName.setAttribute('id', 'LastName');
        inputLastName.setAttribute('style', '');
        inputLastName.setAttribute('for', '');
        inputLastName.setAttribute('maxlength', '50');
        inputLastName.setAttribute('onkeypress', 'return validateNames(event);');
        inputLastName.setAttribute('onpaste', 'return false');
        divInputLastName.appendChild(inputLastName);

        divContainerLastName.appendChild(divIconLastName);
        divContainerLastName.appendChild(divInputLastName);

        divHeadCanvasA.appendChild(divContainerLastName);

        const divContainerIdentifyType = document.createElement('div');
        divContainerIdentifyType.className = 'row';

        const divIconIdentifyType = document.createElement('div');
        divIconIdentifyType.className = 'div-icon-name col';

        const divIconIdentifyTypeUser = document.createElement('div');
        divIconIdentifyTypeUser.className = 'icon-user-identify';
        divIconIdentifyType.appendChild(divIconIdentifyTypeUser);

        const divTextTypeIdentifyUser = document.createElement('div');
        divTextTypeIdentifyUser.className = 'col div-name-user';
        divTextTypeIdentifyUser.textContent = 'Tipo de documento';
        divIconIdentifyType.appendChild(divTextTypeIdentifyUser);

        const divInputIdentifyType = document.createElement('div');
        divInputIdentifyType.className = 'mb-3 col';

        const typesIdentify = `<option>Tipo</option>
        <option value="RC">Registro Civil</option>
        <option value="TI">Tarjeta de Identidad</option>
        <option value="CC">C&eacute;dula de Ciudadan&iacute;a</option>
        <option value="CE">C&eacute;dula de Extranjer&iacute;a</option>
        <option value="PASAPORTE">Pasaporte</option>
        <option value="NIT">NIT</option>
        <option value="PED">PEP</option>`;

        const selectIdentify = document.createElement('select');
        selectIdentify.className = 'form-select col select-document';
        selectIdentify.setAttribute('type', 'select-one');
        selectIdentify.setAttribute('onclick', 'validateTipeDoc(event)');
        selectIdentify.id = 'TipoDocumento';
        selectIdentify.innerHTML = typesIdentify;
        divInputIdentifyType.appendChild(selectIdentify);

        divContainerIdentifyType.appendChild(divIconIdentifyType);
        divContainerIdentifyType.appendChild(divInputIdentifyType);

        divHeadCanvasA.appendChild(divContainerIdentifyType);

        // Se añaden todos los hijos de divHeadCanvasB
        //Name

        const divContainerIdentify = document.createElement('div');
        divContainerIdentify.className = 'row';

        const divIconIdentify = document.createElement('div');
        divIconIdentify.className = 'div-icon-name mb-3 col';

        const divIconIdentifyUser = document.createElement('div');
        divIconIdentifyUser.className = 'icon-user-identify';
        divIconIdentify.appendChild(divIconIdentifyUser);

        const divTextIdentifyUser = document.createElement('div');
        divTextIdentifyUser.className = 'col div-name-user';
        divTextIdentifyUser.textContent = 'Número de documento';
        divIconIdentify.appendChild(divTextIdentifyUser);

        const divInputIdentify = document.createElement('div');
        divInputIdentify.className = 'mb-3 col';

        const inputIdentify = document.createElement('input');
        inputIdentify.className = 'form-control input-border';
        inputIdentify.setAttribute('type', 'text');
        inputIdentify.setAttribute('id', 'Identify');
        inputIdentify.setAttribute('style', '');
        inputIdentify.setAttribute('for', '');
        inputIdentify.setAttribute('maxlength', '50');
        inputIdentify.setAttribute('onkeypress', 'return validateIdentify(event);');
        inputIdentify.setAttribute('onpaste', 'return false');
        divInputIdentify.appendChild(inputIdentify);

        divContainerIdentify.appendChild(divIconIdentify);
        divContainerIdentify.appendChild(divInputIdentify);

        divHeadCanvasB.appendChild(divContainerIdentify);

        const divContainerPhone = document.createElement('div');
        divContainerPhone.className = 'row';

        const divIconPhone = document.createElement('div');
        divIconPhone.className = 'div-icon-name mb-3 col';

        const divIconPhoneIcon = document.createElement('div');
        divIconPhoneIcon.className = 'icon-user-phone';
        divIconPhone.appendChild(divIconPhoneIcon);

        const divTextPhone = document.createElement('div');
        divTextPhone.className = 'col div-name-user';
        divTextPhone.textContent = 'Celular';
        divIconPhone.appendChild(divTextPhone);

        const divInputPhone = document.createElement('div');
        divInputPhone.className = 'mb-3 col';

        const inputPhone = document.createElement('input');
        inputPhone.className = 'form-control input-border';
        inputPhone.setAttribute('type', 'text');
        inputPhone.setAttribute('id', 'PhoneNumber');
        inputPhone.setAttribute('style', '');
        inputPhone.setAttribute('for', '');
        inputPhone.setAttribute('maxlength', '50');
        inputPhone.setAttribute('onkeypress', 'return validatePhoneVideo(event);');
        inputPhone.setAttribute('onpaste', 'return false');
        divInputPhone.appendChild(inputPhone);

        divContainerPhone.appendChild(divIconPhone);
        divContainerPhone.appendChild(divInputPhone);

        divHeadCanvasB.appendChild(divContainerPhone);

        const divContainerEmail = document.createElement('div');
        divContainerEmail.className = 'row';

        const divIconEmail = document.createElement('div');
        divIconEmail.className = 'div-icon-name mb-3 col';

        const divIconEmailUser = document.createElement('div');
        divIconEmailUser.className = 'icon-user-email';
        divIconEmail.appendChild(divIconEmailUser);

        const divTextEmailUser = document.createElement('div');
        divTextEmailUser.className = 'col div-name-user';
        divTextEmailUser.textContent = 'Correo';
        divIconEmail.appendChild(divTextEmailUser);

        const divInputEmail = document.createElement('div');
        divInputEmail.className = 'mb-3 col';

        const inputEmail = document.createElement('input');
        inputEmail.className = 'form-control input-border';
        inputEmail.setAttribute('type', 'text');
        inputEmail.setAttribute('id', 'EmailAddress');
        inputEmail.setAttribute('style', '');
        inputEmail.setAttribute('for', '');
        inputEmail.setAttribute('maxlength', '50');
        inputEmail.setAttribute('onkeypress', 'return validateEmailKey(event);');
        inputEmail.setAttribute('onpaste', 'return false');
        divInputEmail.appendChild(inputEmail);

        divContainerEmail.appendChild(divIconEmail);
        divContainerEmail.appendChild(divInputEmail);

        divHeadCanvasB.appendChild(divContainerEmail);

        const volumeControl = document.createElement('input');
        volumeControl.setAttribute('type', 'range');
        volumeControl.setAttribute('id', 'volume-control');

        const videoAu = document.getElementById('localView');

        divInconsMedia.appendChild(volumeControl);

        const audio = new Audio(videoAu);

        const volume = document.querySelector('#volume-control');
        volume.addEventListener('change', function(e) {
            audio.volume = e.currentTarget.value / 100;
        });


        const bannerDivFooter = document.createElement('div');
        bannerDivFooter.className = 'container-banner-footer mb-2';
        const textbannerDivFooter = document.createElement('div');

        textbannerDivFooter.textContent = 'Bienvenido a nuestro servicio  de atención';
        bannerDivFooter.appendChild(textbannerDivFooter);

        form.appendChild(bannerDivFooter);

        const divContainerAsunto = document.createElement('div');
        divContainerAsunto.className = 'col-12 mt-1 mb-4';

        const inputAsunto = document.createElement('textarea');
        inputAsunto.className = 'form-control input-border asunt';
        inputAsunto.setAttribute('type', 'text');
        inputAsunto.setAttribute('id', 'LastName');
        inputAsunto.setAttribute('style', '');
        inputAsunto.setAttribute('for', '');
        inputAsunto.setAttribute('maxlength', '50');
        inputAsunto.setAttribute('onkeypress', 'return validateNames(event);');
        inputAsunto.setAttribute('onpaste', 'return false');
        divContainerAsunto.appendChild(inputAsunto);

        form.appendChild(divContainerAsunto);

        const divRowCheck = document.createElement('div');
        divRowCheck.className = 'col-12 mb-4';

        const divContainerRowCheck = document.createElement('div');
        divContainerRowCheck.className = 'col d-flex justify-content-center';
        divRowCheck.appendChild(divContainerRowCheck);

        const divContainerElementRowCheck = document.createElement('div');
        divContainerElementRowCheck.className = 'form-check';
        divContainerRowCheck.appendChild(divContainerElementRowCheck);

        const inputCheckPol = document.createElement('input');
        inputCheckPol.className = 'form-check-input';
        inputCheckPol.setAttribute('type', 'checkbox');
        inputCheckPol.setAttribute('id', 'Politicas');
        inputCheckPol.setAttribute('value', '');
        inputCheckPol.setAttribute('required', true);
        divContainerElementRowCheck.appendChild(inputCheckPol);

        const labelCheckPol = document.createElement('a');
        labelCheckPol.className = 'form-check-label';
        labelCheckPol.setAttribute('href', 'https://alpina.com/terminos-y-condiciones');
        labelCheckPol.setAttribute('target', '_blank');
        labelCheckPol.innerHTML = 'Conoce y acepta los  Términos y condiciones';
        divContainerElementRowCheck.appendChild(labelCheckPol);

        const brlabelCheckPol = document.createElement('br');
        divContainerElementRowCheck.appendChild(brlabelCheckPol);

        const divInvalidCheck = document.createElement('div');
        divInvalidCheck.className = 'invalid-feedback';
        divInvalidCheck.innerText = 'Para continuar es necesario leer, entender y autorizar  la Política los Términos y Condiciones, la Política de Tratamiento de Datos.';
        divContainerElementRowCheck.appendChild(divInvalidCheck);

        form.appendChild(divRowCheck);

        const divRowCheckTwo = document.createElement('div');
        divRowCheckTwo.className = 'col-12 mb-4';

        const divContainerRowCheckTwo = document.createElement('div');
        divContainerRowCheckTwo.className = 'col d-flex justify-content-center';
        divRowCheckTwo.appendChild(divContainerRowCheckTwo);

        const divContainerElementRowCheckTwo = document.createElement('div');
        divContainerElementRowCheckTwo.className = 'form-check';
        divContainerRowCheckTwo.appendChild(divContainerElementRowCheckTwo);

        const inputCheckPolTwo = document.createElement('input');
        inputCheckPolTwo.className = 'form-check-input';
        inputCheckPolTwo.setAttribute('type', 'checkbox');
        inputCheckPolTwo.setAttribute('id', 'Politicas');
        inputCheckPolTwo.setAttribute('value', '');
        inputCheckPolTwo.setAttribute('required', true);
        divContainerElementRowCheckTwo.appendChild(inputCheckPolTwo);

        const labelCheckPolTwo = document.createElement('a');
        labelCheckPolTwo.className = 'form-check-label';
        labelCheckPolTwo.setAttribute('href', 'https://alpina.com/static/version1650895862/frontend/Omnipro/alpina/default/docs/tratamiento-datos/Formato-Autorizaci%C3%B3n-Tratamientos-de-Datos-en-Medios-Digitales.pdf');
        labelCheckPolTwo.setAttribute('target', '_blank');
        labelCheckPolTwo.innerHTML = 'Autorizo y declaro que soy mayor de edad, que he leído y acepto el tratamiento de mis datos personales conforme al formato de autorización disponible aquí';
        divContainerElementRowCheckTwo.appendChild(labelCheckPolTwo);

        const brlabelCheckPolTwo = document.createElement('br');
        divContainerElementRowCheckTwo.appendChild(brlabelCheckPolTwo);

        const divInvalidCheckTwo = document.createElement('div');
        divInvalidCheckTwo.className = 'invalid-feedback';
        divInvalidCheckTwo.innerText = 'Para continuar es necesario leer, entender y autorizar  la Política los Términos y Condiciones, la Política de Tratamiento de Datos.';
        divContainerElementRowCheckTwo.appendChild(divInvalidCheckTwo);

        form.appendChild(divRowCheckTwo);

        const divMedios = document.createElement('div');
        divMedios.className = 'modal_medios';
        divMedios.className = 'invalid-feedback';
        divMedios.setAttribute('id', 'medios-si');

        const brMedios = document.createElement('br');
        divMedios.appendChild(brMedios);
        const h6 = document.createElement('h6');
        h6.className = 'modal_medios';
        h6.setAttribute('id', 'mensaje');
        divMedios.appendChild(h6);
        divContainerElementRowCheck.appendChild(divMedios);

        const divFooterButtonForm = document.createElement('div');
        divFooterButtonForm.className = 'footer-custom col-12';
        form.appendChild(divFooterButtonForm);

        const buttonSendForm = document.createElement('button');
        buttonSendForm.className = 'btn btn-success';
        buttonSendForm.setAttribute('onclick', 'validateForm()');
        buttonSendForm.setAttribute('type', 'button');
        buttonSendForm.setAttribute('data-mdb-toggle', 'tooltip');
        buttonSendForm.setAttribute('data-mdb-placement', 'bottom');
        buttonSendForm.setAttribute('title', 'Llamar');
        const iconCall = document.createElement('i');
        iconCall.className = 'fas fa-phone';
        buttonSendForm.appendChild(iconCall);
        divFooterButtonForm.appendChild(buttonSendForm);

        const divFooterModal = document.createElement('div');
        divFooterModal.className = 'footer-custom col-12';
        divForm.appendChild(divFooterModal);

        const btnEndCall = document.createElement('button');
        btnEndCall.className = 'btn btn-danger btn-end-call';
        btnEndCall.setAttribute('type', 'button');
        btnEndCall.setAttribute('onclick', 'terminateCall()');
        btnEndCall.setAttribute('aria-label', 'Close');
        btnEndCall.setAttribute('data-bs-dismiss', 'offcanvas');
        btnEndCall.setAttribute('data-mdb-toggle', 'tooltip');
        btnEndCall.setAttribute('data-mdb-placement', 'button');
        btnEndCall.setAttribute('title', 'Colgar');
        btnEndCall.setAttribute('hidden', true);
        const iconEndCall = document.createElement('i');
        iconEndCall.className = 'fas fa-phone-slash';
        btnEndCall.appendChild(iconEndCall);

        divFooterModal.appendChild(btnEndCall);

        const spanLocalStatus = document.createElement('span');
        spanLocalStatus.setAttribute('id', 'localStatus');
        spanLocalStatus.setAttribute('class', 'form-text');
        spanLocalStatus.setAttribute('hidden', true);

        const spanRemoteStatus = document.createElement('span');
        spanRemoteStatus.setAttribute('id', 'remoteStatus');
        spanRemoteStatus.setAttribute('class', 'form-text');
        spanRemoteStatus.setAttribute('hidden', true);

        divCanvasBody.appendChild(spanLocalStatus);
        divCanvasBody.appendChild(spanRemoteStatus);

    }

    const script = document.createElement('script');
    script.src = 'js/mdb.min.js';

    document.head.appendChild(script);
});