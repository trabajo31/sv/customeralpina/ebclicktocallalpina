let connectedSesion = false;
let grtcSession = null;
let grtcClient = null;
let ip = null;
let IDsession = null;
var userData;

function uuidv4() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}

IDsession = uuidv4();


const soloLetras = function(e) {
    const key = e.keyCode || e.which;
    const tecla = String.fromCharCode(key).toLowerCase();
    const expression = /^[A-Za-z\_\s\xF1\xD1]+$/;
    return (expression.test(tecla));
};

const validateNames = e => {
    const validar = soloLetras(e);
    if(validar) {
        $('#FirstName').removeClass("is-invalid");
    }
    return validar;
};

const validateTipeDoc = function() {
    const TipeDoc = document.getElementById('TipoDocumento');
    if(TipeDoc.value !== 'Tipo') {
        TipeDoc.classList.remove('is-invalid');
    } else {
        TipeDoc.classList.add('is-invalid');
    }
};

const validateEmailKey = e => {
    const valid = sinEspecialesEmail(e);
    if(valid) {
        $('#EmailAddress').removeClass("is-invalid");
    }

    const email = document.getElementById('EmailAddress');

    if(!validateEmailVideo(email.value)) {
        $('#EmailAddress').addClass("is-invalid");
    }
    return valid;
};

const validateEmailVideo = function(mail) {
    /**
     * INFORMACIÓN PARA EL EQUIPO DE SEGURIDAD
     * Esta es una validación que hace parte de la logica de negocio
     */
    return (/.+\@.+\..+/gm.test(mail));
};

const validatePhoneVideo = e => {
    const valid = valideKey(e);
    if(valid) {
        $('#PhoneNumber').removeClass("is-invalid");
    }
    return valid;
};

const validateIdentify = e => {
    const valid = valideKey(e);
    if(valid) {
        $('#Identify').removeClass("is-invalid");
    }
    return valid;
};

const sinEspecialesEmail = function(e) {
    const key = e.keyCode || e.which;
    const tecla = String.fromCharCode(key).toLowerCase();
    const expression = /^[()<>,;:"\[\]ç%&]/;
    return !(expression.test(tecla));
};

const valideKey = function(evt) {

    // code is the decimal ASCII representation of the pressed key.
    const code = (evt.which) ? evt.which : evt.keyCode;
    if (code === 8) { // backspace.
        return true;
    } else if (code >= 48 && code <= 57) { // is a number.
        return true;
    } else { // other keys.
        return false;
    }
};


var conf = {
    'webrtc_gateway': 'https://webrtc.grupokonecta.co:8086',
    "stun_server": "stun.l.google.com:19302",
    "dtls_srtp" : true
};

/**
 * INFORMACIÓN PARA EL EQUIPO DE SEGURIDAD
 * Esta es una variable de la librería, es llamada de un archivo ubicado en lib/genesys/grtc.js
 * Solo podemos llamarla de esta manera porque no debemos modigicar la librería
 */
grtcClient = new Grtc.Client(conf);

// add a handler to do some work when the peer closes
grtcClient.onPeerClosing.add(function () {
    $("#remoteStatus").empty();
    if (grtcSession) grtcSession = null;
});

grtcClient.onMediaSuccess.add(function() {

    grtcClient.onConnect.add(function() {
        $('#localStatus').empty();
        $('#localStatus').append('conectado');
        grtcSession = new Grtc.MediaSession(grtcClient);
        grtcSession.setData(userData);

        grtcSession.onRemoteStream.add(function(data) {
            grtcClient.setViewFromStream(document.getElementById('remoteView'), data.stream);
        });
        const skill = '9949999974';
        // const skill = '9949999990';
        grtcSession.makeOffer(skill, true, false);
    });

    grtcClient.onFailed.add(() => {
        const divContainerElementRowCheck = document.getElementById('medios-si');
        const mensaje = document.getElementById('mensaje');
        mensaje.innerText = 'No se pudo establecer la conexión';
        divContainerElementRowCheck.classList.remove('invalid-feedback');

        setTimeout(() => {
            divContainerElementRowCheck.classList.add('invalid-feedback');
        },10000);
    });

});

grtcClient.onMediaFailure.add(function() {
    const divContainerElementRowCheck = document.getElementById('medios-si');
    divContainerElementRowCheck.classList.remove('invalid-feedback');
    const mensaje = document.getElementById('mensaje');
    mensaje.innerText = 'No se pudo acceder a los medios, micrófono';
    setTimeout(() => {
        divContainerElementRowCheck.classList.add('invalid-feedback');
    },10000);
});
// enable microphone and camera
grtcClient.enableMediaSource(true, false);

window.onbeforeunload = function() {
    grtcClient.disconnect();
};

async function getIpClient() {
    try {
        const response = await axios.get('https://ipinfo.io/json');
        ip = response.data.ip;
    } catch (error) {
        //Error al optener la ip
    }
}


$(function() {
    $('[data-toggle="tooltip"]').tooltip();
    getIpClient();
});

function validateForm() {

    let formValid = true;

    const user = [];

    const elements = document.getElementById('form_init').elements;

    for (const element of elements){

        if (element.type === 'select-one' && element.value === '') {
            element.classList.add('is-invalid');
            formValid = false;
            break;
        } else if (element.type === 'select-one' && element.value !== '') {
            element.classList.add('was-validated');
            user.push({
                key: element.id,
                value: element.value
            });
        } else {
            //
        }


        if(element.id === 'TipoDocumento' && element.value === 'Tipo') {
            element.classList.add('is-invalid');
            formValid = false;
            break;
        } else {
            element.classList.remove('is-invalid');
        }

        if (element.type === 'text' && element.value === '') {
            element.classList.add('is-invalid');
            formValid = false;
            break;
        } else if (element.type === 'text' && element.value !== '') {
            element.classList.add('was-validated');
            user.push({
                key: element.id,
                value: element.value
            });
        } else {
            element.classList.remove('is-invalid');
        }

        if (element.type === 'email' && element.value === '') {
            element.classList.add('is-invalid');
            formValid = false;
            break;
        } else if (element.type === 'email' && element.value !== '') {
            if (validateEmailVideo(element.value)) {
                element.classList.add('was-validated');
                user.push({
                    key: element.id,
                    value: element.value
                });
            } else {
                element.classList.add('is-invalid');
                formValid = false;
            }
        } else {
            //
        }

        if (element.type === 'tel' && element.value === '') {
            element.classList.add('is-invalid');
            formValid = false;
            break;
        } else if (element.type === 'tel' && element.value !== '') {
            element.classList.add('was-validated');
            user.push({
                key: element.id,
                value: element.value
            });
        } else {
            //
        }


    }

    if (formValid) {

        user.push({
            key: 'IpOrigen',
            value: ip
        });

        user.push({
            key: 'Subject',
            value: 'EbVideoCallAlpina'
        });

        user.push({
            key: 'ID_Session',
            value: IDsession
        });

        const identifyConcat = user[1].value + '-' + user[2].value;
        user[2].value = identifyConcat;



        setConfigCall(user);

    } else {
        const divContainerElementRowCheck = document.getElementById('medios-si');
        const mensaje = document.getElementById('mensaje');
        mensaje.innerText = 'Ingrese los datos de manera correcta';
        divContainerElementRowCheck.classList.remove('invalid-feedback');
        setTimeout(() => {
            divContainerElementRowCheck.classList.add('invalid-feedback');
        },10000);
    }
}

function setConfigCall(userCall) {
    userData = userCall;
    grtcClient.connect();
    connectedSesion = true;
    hiddeShowElements();
}

//Función para finalizar llamada
function terminateCall() {
    if (connectedSesion) {
        grtcSession.terminateCall();
        grtcSession = null;
        $('#remoteStatus').empty();
        const div_canvas = document.getElementById('offcanvasScrolling');
        div_canvas.classList.remove('call-start');

        document.getElementById('form_init').reset();
        grtcClient.disableMediaSource();
        hiddeShowElementsEndCall();
    } else {
        //
    }
}


function hiddeShowElements() {
    const form = document.getElementById('form_init');
    form.hidden = true;
    const controls = document.getElementsByClassName('controls');
    controls[0].hidden = false;

    const btnEnd = document.getElementsByClassName('btn btn-danger');
    btnEnd[0].hidden = false;

    const localStatus = document.getElementById('localStatus');
    localStatus.hidden = false;

    const remoteStatus = document.getElementById('remoteStatus');
    remoteStatus.hidden = false;

    const div_canvas = document.getElementById('offcanvasScrolling');
    div_canvas.classList.add('call-start');
}

//Mostrar nuevamente formulario después de finalizar la llamada
function hiddeShowElementsEndCall() {
    const form = document.getElementById('form_init');
    form.hidden = false;
    const controls = document.getElementsByClassName('controls');
    controls[0].hidden = true;

    const btnEnd = document.getElementsByClassName('btn btn-danger');
    btnEnd[0].hidden = true;

    const localStatus = document.getElementById('localStatus');
    localStatus.hidden = true;

    const remoteStatus = document.getElementById('remoteStatus');
    remoteStatus.hidden = true;


}

//Funcion para Validar el Control de Horario.
function ControlHorario() {
    var controlhvalid = true;
    var fecha = new Date();
    var semana = fecha.getDay();
    var hora = fecha.getHours();
    var semanahoy = parseInt(semana) + parseInt('1');
    semana = semanahoy;

    if (semana == 1) {
        if(hora < 8 || hora >= 17){
            controlhvalid = false;
        }
    } else if (semana == 7) {
        if (hora < 8 || hora >= 19) {
            controlhvalid = false;
        }
    } else if (hora < 7 || hora >= 21) {
        controlhvalid = false;
    } else {
        //control de horario inválido
    }
    return controlhvalid;
}