
# MANUAL TÉCNICO 

## **EbClickToCallAlpina**
## Servicio Virtual 



## Control de cambios:


| Fecha | Autor | Versión | Referencia de cambio |
| -------- | -------- | -------- | -------- |
| 12/04/2022 | Luis Manuel Hernandez Jimenez | 1 | [ALPN-8](https://pmo.konecta.cloud/browse/ALPN-8) |


# Índice

* [Introducción](#introducción)
* [Infraestructura](#infraestructura)
* [Arquitectura](#arquitectura)
* [Configuraciones de montaje](#configuraciones-de-montaje)
  - [Requerimientos mínimos de hardware:](#requerimientos-mínimos-de-hardware)
  - [Requerimientos mínimos de software:](#requerimientos-mínimos-de-software)
  - [Afinación de configuración para el proyecto:](#afinación-de-configuración-para-el-proyecto)
* [Tipos de usuarios](#tipos-de-usuarios)
* [Estructura código fuente](#estructura-código-fuente)
  - [Lenguaje de programación:](#lenguaje-de-programación)
  - [Esquema de directorios](#esquema-de-directorios)
  - [Descripción del flujo:](#descripción-del-flujo)
  - [Integraciones](#integraciones)
  - [Nuevas funcionalidades:](#nuevas-funcionalidades)
* [Manejo de datos](#manejo-de-datos)
  - [Modelo Entidad-Relación:](#modelo-entidad-relación)
  - [Diccionario de datos:](#diccionario-de-datos)
* [Bibliografía](#bibliografía)


# [Introducción](#introducción)

Este documento contiene la información técnica del servicio virtual **EbClickToCallAlpina**,  un  servicio virtual  es construido  con la finalidad de establecer un contacto con un asesor en línea para solicitar un servicio o producto ofrecido por la empresa,  accediendo a través de  una URL y por medio de un navegador web, este contacto  es logrado mediante la previa configuración, integración de los componentes de desarrollo, y su posterior implementación.

Este documento describe la información referente al desarrollo, Infraestructura y aspectos a tener en cuenta para la puesta en marcha de este servicio virtual. 


# [Infraestructura](#infraestructura)

**En la figura 1** , se presenta  gráficamente los elementos físicos  que intervienen en el funcionamiento de los servicios virtuales.

![image](/uploads/16de6cfeb86bab532c43401efc803e6c/image.png)

_Figura 1: "Diagrama de Esquema Servicios Virtuales”_

# [Arquitectura](#arquitectura)

La arquitectura de los servicios virtuales está basada en el paradigma de  programación orientada a objetos y bajo el esquema de arquitectura cliente-servidor, el cual permite administrar de manera independiente los componentes que se ubican en el nivel más externo; la capa de presentación,  la lógica de negocio , y la persistencia a datos.

En este caso los componentes del modelo están conformados por clases, objetos, métodos entre otros elementos de infraestructura física y lógica.

**En la figura 2**, se muestra una representación gráfica breve de  los componentes que intervienen en la ejecución de los servicios virtuales.

![image](/uploads/a4271f885bfdcc8ea9c0b95948991a10/image.png)

_Figura 2: "Diagrama de Arquitectura"_

# [Configuraciones de montaje](#configuraciones-de-montaje)

## [Requerimientos mínimos de hardware:](#requerimientos-mínimos-de-hardware)

Las características mínimas de hardware recomendadas en el entorno de ejecución son:

-   Procesador Intel pentium 4  de 32 nucleos
-   Memoria RAM de 4 gb en adelante hasta 32 gb
-   99 gb de Disco Duro

Las características minimas de hardware de los equipos cliente son:

-   Procesador Pentium III
-   Sistema operativo: Unix, Linux, Windows, BSD
-   Consumo de Memoria: 128MB
-   Consumo de Procesador: 400MHZ

## [Requerimientos mínimos de software:](#requerimientos-mínimos-de-software)

-   Genesys Workspace Desktop Edition 8,5
-   Navegador Web Google Chrome  preferiblemente versión 5,0 en adelante
-   Oracle 11g Release 11,2,0,4,0

## [Afinación de configuración para el proyecto:](#afinación-de-configuración-para-el-proyecto)

-   Configuración conexión bases de datos
-   Definición de  rutas base URL y referencias
-   Configuración de los directorios donde se alojan los archivos
-   Conexión a Internet
-   Habilitar complementos

# [Tipos de usuarios](#tipos-de-usuarios)

Los servicios virtuales no implementan en su estructura un control de acceso con password y login, el componente al inicio pide al usuario su información básica personal con su nombre, apellido, tipo documento, numero documento, correo electrónico y en la mayoría de las veces su numero de móvil. Con estos datos se logra identificar al cliente o usuario.

# [Estructura código fuente](#estructura-código-fuente)

## [Lenguaje de programación:](#lenguaje-de-programación)

Las tecnologías utilizadas para la implementación de los servicios virtuales son los siguientes:

**Back-End**
-   JSP(Java Server Page)

 **Front-End**
-   Jquery Framework (Api de javascript)
-   Javascript (Lenguaje de Programación Interpretado)
-   Html (Lenguaje de Marcas de Hipertexto)
-   Css (Hojas de estilo en cascada)

## [Esquema de directorios:](#esquema-de-directorios)

Todos los archivos necesarios para el funcionamiento del servicio virtual se  encuentran en los siguientes directorios:

**EbClickToCallAlpina Archivos ubicados en las rutas de desarrollo:**

- \\172.102.180.177\DesarrolloSV1\CustomerAlpina\EbClickToCallAlpina\index.html
-  https://asistenciawebv2-dev.grupokonecta.local/CustomerAlpina/EbClickToCallAlpina/index.html

## [Descripción del flujo](#descripción-del-flujo)

-  https://asistenciawebv2.grupokonecta.co:8443/EbClickToCallAlpina/index.html

## [Integraciones](#(#integraciones))

N/A

## [Nuevas funcionalidades](#nuevas-funcionalidades)


| Nombre Desarrollador: | **Luis Hernandez** |
| ------------------- | -------- | 
| Función que soluciona (descripción del objetivo del desarrollo, añadir código de petición o TK) | **Se resuelven las incidencias reportadas por seguridad<br> Flujo principal inicial por el archivo index.html** |
| Archivos afectados: | **Mencionados en el ítem Esquema de Directorios** |
| Reglas de negocio aplicadas: | **Ninguna** |
| Descripción de la solución: | **Actualización de EbClickToCallAlpina** |
| Afectación de bases de datos (script - Si lo considera, añadirlo como anexo al final del documento): | **Base de Datos LimeSurvey alojada en el servidor 172.20.73.21, Base de datos GENESYS_WC alojada en el servidor 172.20.1.72** |

# [Manejo de datos](#manejo-de-datos)

## [Modelo Entidad-Relación:](#modelo-entidad-relación)



# [Diccionario de datos](#diccionario-de-datos)

**Tabla LLAMADA** : Almacena la información detallada de la llamada.

|**Descripción Campo**|**Tipo de Dato**|**Comentarios**|
|---------------------|----------------|---------------|
|CONNID|VARCHAR2(32 BYTE)|Almacena el Id del Registro|
|CONNID_BANCO|VARCHAR2(32 BYTE)|Almacena el Id del Banco|
|AGALLUS_USER_NAME|VARCHAR2(50 BYTE)|Almacena el Nombre del Agente|
|INI_CALL|VARCHAR2(50 BYTE)|Almacena información de la Interacción inicial|
|DUR_CALL|VARCHAR2(50 BYTE)|Almacena la Duración de la Llamada|
|END_CALL|VARCHAR2(50 BYTE)|Almacena información de la finalización de la llamada|
|INI_CALL_2|NUMBER|Bandera que contiene el estado de la llamada inicial|

**Tabla U_EMPRESA** : Almacena la información del detalle del Encargado de atender la llamada.

|**Descripción Campo**|**Tipo de Dato**|**Comentarios**|
|---------------------|----------------|---------------|
|CODEMP|VARCHAR2(3 BYTE)|Almacena el código del empleado|
|DESCEMP|VARCHAR2(64 BYTE)|Almacena la Información de la descripción del Cargo Empleado|

**Tabla U_PRODUCTOS** : Almacena la Información de los productos o Servicios que ofrece la empresa.

|**Descripción Campo**|**Tipo de Dato**|**Comentarios**|
|---------------------|----------------|---------------|
|CODPROD|VARCHAR2(6 BYTE)|Almacena el código del producto|
|CODSUPROD|VARCHAR2(6 BYTE)|Almacena el Código del Subproducto|
|DESCCOD|VARCHAR2(64 BYTE)|Almacena la información de la Descripción del código Producto|
|CODEMP|VARCHAR2(3 BYTE)|Almacena el código del empleado|
|INB2_OUT3|VARCHAR2(1 BYTE)|Almacena el estado del producto|
|DESCPROD|VARCHAR2(64 BYTE)|Almacena la descripción del producto|

**Tabla DETALLE_LLAMADA** : Almacena la información del detalle de la llamada.

|**Descripción Campo**|**Tipo de Dato**|**Comentarios**|
|---------------------|----------------|---------------|
|CONNID|VARCHAR2(32 BYTE)|Almacena el id automático generado por la base de datos|
|CONNID_BANCO|VARCHAR2(32 BYTE)|Almacena el id automático generado por la base de datos del banco|
|AGALLUS_USER_NAME|VARCHAR2(50 BYTE)|Almacena el nombre del agente allus|
|LINEA_ATENCION|VARCHAR2(100 BYTE)|Almacena la información de la línea de atención|
|ID_CATEGORIA|VARCHAR2(50 BYTE)|Almacena el identificador de la categoría|
|CATEGORIA|VARCHAR2(255 BYTE)|Almacena la información de la Descripción de la Categoría|
|ID_MOTIVO|VARCHAR2(50 BYTE)|Almacena la información del identificador del motivo|
|MOTIVO|VARCHAR2(255 BYTE)|Almacena la Descripción del Motivo|
|TIPO_DE_DOCUMENTO|VARCHAR2(50 BYTE)|Almacena el id del tipo de documento|
|NUMERO_DE_DOCUMENTO|VARCHAR2(20 BYTE)|Almacena la información del numero de documento|
|ID_DEL_CLIENTE|VARCHAR2(32 BYTE)|Almacena la información de la identificación del cliente|
|NOMBRE_DEL_CLIENTE|VARCHAR2(100 BYTE)|Almacena el nombre del cliente|
|SEGMENTO|VARCHAR2(50 BYTE)|Almacena la información del segmento|
|SUBSEGMENTO|VARCHAR2(50 BYTE)|Almacena la información del Subsegmento|
|FECHA_NACIMIENTO|VARCHAR2(20 BYTE)|Almacena la fecha de Nacimiento del cliente|
|CIUDAD_DEL_CLIENTE|VARCHAR2(50 BYTE)|Almacena la información de la Ciudad del cliente|
|FECHA_CREACION_ACTIVIDAD|VARCHAR2(20 BYTE)|Almacena la información del registro de la actividad|
|HORA_CREACION_ACTIVIDAD|VARCHAR2(20 BYTE)|Almacena la información de la hora del  registro de la actividad|
|AGENTE_IC|VARCHAR2(50 BYTE)|Almacena la información del detalle del agente|
|AG_BDID|VARCHAR2(10 BYTE)|Almacena la información del detalle del agente| 
|AG_FIRST_NAME|VARCHAR2(255 BYTE)|Almacena la información del detalle del agente|
|AG_LAST_NAME|VARCHAR2(255 BYTE)|Almacena la información del detalle del agente| 
|AG_LOGIN_CODE|VARCHAR2(5 BYTE)|Almacena la información del detalle del agente|
|PL_EXTENSION|VARCHAR2(5 BYTE)|Sin Descripción|
|PL_ACDPOS|VARCHAR2(5 BYTE)|Sin Descripción|
|PL_PLACE|VARCHAR2(25 BYTE)|Sin Descripción|
|CTI_TIPORI|VARCHAR2(1 BYTE)|Almacena la información del tipo de origen del Servicio Virtual|
|CTI_CODORI|VARCHAR2(5 BYTE)|Almacena la información del  código  de origen del Servicio Virtual|
|CTI_ANI|VARCHAR2(20 BYTE)|Sin Descripción|
|CTI_DNIS|VARCHAR2(20 BYTE)|Sin Descripción| 
|CTI_REFID|VARCHAR2(32 BYTE)|Sin Descripción| 
|CTI_INDUST|VARCHAR2(2 BYTE)|Sin Descripción| 
|CTI_INSTIT|VARCHAR2(2 BYTE)|Sin Descripción|
|CTI_APLICA|VARCHAR2(2 BYTE)|Sin Descripción|
|CTI_ID|VARCHAR2(50 BYTE)|Sin Descripción|
|CTI_TIPIDE|VARCHAR2(3 BYTE)|Sin Descripción|
|CTI_NOMBRE|VARCHAR2(255 BYTE)|Sin Descripción|
|CTI_DIRECC1|VARCHAR2(255 BYTE)|Sin Descripción|
|CTI_TELEF1|VARCHAR2(50 BYTE)|Sin Descripción| 
|CTI_FAX1|VARCHAR2(50 BYTE)|Sin Descripción| 
|CTI_FECHNA|VARCHAR2(50 BYTE)|Sin Descripción| 
|CALL_COD_CIUDAD|VARCHAR2(3 BYTE)|Sin Descripción| 
|CTI_FECHVI|VARCHAR2(50 BYTE)|Sin Descripción|
|CTI_FECHAC|VARCHAR2(50 BYTE)|Sin Descripción|
|CTI_CLAVESN|VARCHAR2(1 BYTE)|Sin Descripción|
|CHA_OPC_MENU|VARCHAR2(50 BYTE)|Sin Descripción|
|REGLANEGOCIO|VARCHAR2(2 BYTE)|Sin Descripción|
|TRANSFER|VARCHAR2(35 BYTE)|Sin Descripción|
|GSW_CALLING_LIST|VARCHAR2(255 BYTE)|Sin Descripción|
|INI_CALL_2" NUMBER|VARCHAR2(255 BYTE)|Sin Descripción|
|CALL_CODEMP" VARCHAR2(5 BYTE)|VARCHAR2(5 BYTE)|Sin Descripción|
|NOTA|VARCHAR2(1024 BYTE)|Sin Descripción|

# [Bibliografía](#bibliografía)
 
https://www.oracle.com/co/index.html

https://docs.genesys.com/Documentation/GIM/8.5.0/Dep/GIMNew850

https://www.it.uc3m.es/labttlat/2007-08/material/jsp/

https://jquery.com/

https://css-tricks.com/
